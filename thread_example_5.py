''' Race conditions '''
import urllib.request
import threading
import tkinter as tk

URL_JV = 'https://en.wikipedia.org/wiki/John_von_Neumann'
URL_AT = 'https://en.wikipedia.org/wiki/Alan_Turing'
URL_GH = 'https://en.wikipedia.org/wiki/Grace_Hopper'

def url_getter(a_url, text_control):
	''' Square some numbers '''
	print(f'Fetching URL:{a_url}')
	text_control.delete(1.0, tk.END)
	for _ in range(10):
		html = urllib.request.urlopen(a_url).read()
	text_control.insert(tk.END, html)

def t_url_getter(a_url, text_control):
	''' Threaded URL getter '''
	thread_url_getter = threading.Thread(target=url_getter,
										args=(a_url, text_control),
										daemon=True)
	thread_url_getter.start()

def create_text_control(parent):
	''' create a text control to show the operation plan '''
	frame = tk.Frame(parent, padx=5, pady=5)
	frame.columnconfigure(0, weight=1)
	frame.rowconfigure(0, weight=1)

	text_control = tk.Text(frame,
		width=15,
		height=5,
		wrap='none')
	text_control.grid(row=0, column=0, sticky=(tk.NSEW))

	x_scrollbar = tk.Scrollbar(frame, orient='horizontal', command=text_control.xview)
	x_scrollbar.grid(row=1, column=0, sticky=(tk.EW))

	y_scrollbar = tk.Scrollbar(frame, orient='vertical', command=text_control.yview)
	y_scrollbar.grid(row=0, column=1, sticky=(tk.NS))

	text_control.configure(xscrollcommand=x_scrollbar.set)
	text_control.configure(yscrollcommand=y_scrollbar.set)

	return (frame, text_control)

class AnApp:
	''' Simple TkInter app '''
	def __init__(self):
		''' Instance vars '''
		self._window = tk.Tk()
		self._text_control = None
		self._create_widgets()

	def _create_widgets(self):
		''' Create my widgets '''
		self._window.columnconfigure(0, weight=1)
		self._window.rowconfigure(1, weight=1)

		button_frame = tk.Frame(self._window)
		button = tk.Button(button_frame, text='URL 1', command=self.action_get_url_1)
		button.grid(row=0, column=0, padx=5, pady=5, sticky=(tk.W,))

		button = tk.Button(button_frame, text='URL 2', command=self.action_get_url_2)
		button.grid(row=0, column=1, padx=5, pady=5, sticky=(tk.W,))

		button = tk.Button(button_frame, text='URL 3', command=self.action_get_url_3)
		button.grid(row=0, column=2, padx=5, pady=5, sticky=(tk.W,))
		button_frame.grid(row=0, column=0)

		text_frame, self._text_control = create_text_control(self._window)
		text_frame.columnconfigure(0, weight=1)
		text_frame.grid(row=1, column=0, sticky=(tk.NSEW,))

	def run(self):
		''' Run the app '''
		self._window.mainloop()

	def action_get_url_1(self):
		''' Get a URL '''
		t_url_getter(URL_JV, self._text_control)

	def action_get_url_2(self):
		''' Get a URL '''
		t_url_getter(URL_AT, self._text_control)

	def action_get_url_3(self):
		''' Get a URL '''
		t_url_getter(URL_GH, self._text_control)

if __name__ == '__main__':
	AnApp().run()
