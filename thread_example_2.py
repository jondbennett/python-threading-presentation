''' Re-using a function and passing args '''
import time
import threading

def sleepy_looper(sleep_time, name, tag_char):
	''' Simple task '''
	print(f'{name}: start')

	for _ in range(10):
		print(f'{tag_char}', end='', flush=True)
		time.sleep(sleep_time)

	print(f'\n{name}: end')

def run_threads():
	''' Run tasks with threads '''
	# Create the threads
	thread_1 = threading.Thread(target=sleepy_looper, args=(.3, 'Looper 1', '*'))
	thread_2 = threading.Thread(target=sleepy_looper, args=(.5, 'Looper 2', '='))
	thread_3 = threading.Thread(target=sleepy_looper, args=(.7, 'Looper 3', '-'))

	# Start them
	thread_1.start()
	thread_2.start()
	thread_3.start()

	# Wait for completion
	thread_1.join()
	thread_2.join()
	thread_3.join()

if __name__ == '__main__':
	start_time = time.time()
	run_threads()
	duration = time.time() - start_time
	print(f'The tasks completed in {duration:.2f} seconds')
