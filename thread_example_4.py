''' Race conditions '''
import time
from random import randint
import urllib.request
import threading

URL_JV = 'https://en.wikipedia.org/wiki/John_von_Neumann'
URL_AT = 'https://en.wikipedia.org/wiki/Alan_Turing'
URL_GH = 'https://en.wikipedia.org/wiki/Grace_Hopper'

class SquareServer:
	''' A simple server (fake i/o) '''
	def __init__(self):
		''' Setup '''
		self._input = 0
		self._lock = threading.Lock()

	def square(self, a_number, a_url):
		''' Service a quare request '''
		with self._lock:
			self._input = a_number
			_ = urllib.request.urlopen(a_url).read()
			return self._input ** 2

def get_square(server, a_number, a_url):
	''' Square some numbers '''
	time.sleep(1 / randint(1, 10))
	answer = server.square(a_number, a_url)
	print(f'The square of {a_number} is {answer}')

def run_threads():
	''' Run tasks with threads '''
	# Make the server
	square_server = SquareServer()

	# Create the threads
	thread_1 = threading.Thread(target=get_square, args=(square_server, 2, URL_JV))
	thread_2 = threading.Thread(target=get_square, args=(square_server, 3, URL_AT))
	thread_3 = threading.Thread(target=get_square, args=(square_server, 4, URL_GH))

	# Start them
	thread_1.start()
	thread_2.start()
	thread_3.start()

	# Wait for completion
	thread_1.join()
	thread_2.join()
	thread_3.join()

if __name__ == '__main__':
	start_time = time.time()
	run_threads()
	duration = time.time() - start_time
	print(f'The tasks completed in {duration:.2f} seconds')
