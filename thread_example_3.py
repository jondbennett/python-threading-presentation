''' Speed up i/o bound processing '''
import time
import urllib.request
import threading

URL_JV = 'https://en.wikipedia.org/wiki/John_von_Neumann'
URL_AT = 'https://en.wikipedia.org/wiki/Alan_Turing'
URL_GH = 'https://en.wikipedia.org/wiki/Grace_Hopper'

def url_getter(a_url):
	''' Square some numbers '''
	html = urllib.request.urlopen(a_url).read()
	print(f'Size: {len(html)} URL:{a_url}')

def no_threads():
	''' Run with no threads '''
	url_getter(URL_JV)
	url_getter(URL_AT)
	url_getter(URL_GH)

def run_threads():
	''' Run tasks with threads '''
	# Create the threads
	thread_1 = threading.Thread(target=url_getter, args=(URL_JV, ))
	thread_2 = threading.Thread(target=url_getter, args=(URL_AT, ))
	thread_3 = threading.Thread(target=url_getter, args=(URL_GH, ))

	# Start them
	thread_1.start()
	thread_2.start()
	thread_3.start()

	# Wait for completion
	thread_1.join()
	thread_2.join()
	thread_3.join()

if __name__ == '__main__':
	start_time = time.time()
	no_threads()
	duration = time.time() - start_time
	print(f'No threads {duration:.2f} seconds')

	start_time = time.time()
	run_threads()
	duration = time.time() - start_time
	print(f'With threads {duration:.2f} seconds')
