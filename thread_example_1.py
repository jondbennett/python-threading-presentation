''' Simple thread example '''

import time
import threading

def task_1():
	''' Simple task '''
	print('*** 1 *** Start')
	time.sleep(7)
	print('*** 1 *** End')

def task_2():
	''' Simple task '''
	print('=== 2 === Start')
	time.sleep(5)
	print('=== 2 === End')

def task_3():
	''' Simple task '''
	print('--- 3 --- Start')
	time.sleep(3)
	print('--- 3 --- End')

def without_threads():
	''' Run tasks without threads '''
	task_1()
	task_2()
	task_3()

def with_threads():
	''' Run tasks with threads '''
	# Create the threads
	thread_1 = threading.Thread(target=task_1)
	thread_2 = threading.Thread(target=task_2)
	thread_3 = threading.Thread(target=task_3)

	# Start them
	thread_1.start()
	thread_2.start()
	thread_3.start()

	# Wait for completion
	thread_1.join()
	thread_2.join()
	thread_3.join()

if __name__ == '__main__':
	start_time = time.time()
	without_threads()
	duration = time.time() - start_time
	print(f'Without threads, the tasks completed in {duration:.2f} seconds')
	print()

	start_time = time.time()
	with_threads()
	duration = time.time() - start_time
	print(f'Without threads, the tasks completed in {duration:.2f} seconds')
